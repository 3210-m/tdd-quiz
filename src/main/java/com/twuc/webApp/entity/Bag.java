package com.twuc.webApp.entity;

public class Bag {

    private String size;

    public Bag(String size) {
        this.size = size;
    }

    public Bag() {
        this.size = "s";
    }

    public String getSize() {
        return size;
    }
}
