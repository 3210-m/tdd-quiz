package com.twuc.webApp.entity;


import com.twuc.webApp.enums.SizeEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {

    private int capacity;
    private Map<Ticket, Bag> storage = new HashMap<>();
    private static Map<String, Integer> slots = new HashMap<>();

    public Storage(int capacity) {
        this.capacity = capacity;
        slots.put(SizeEnum.SMALL.getSize(), capacity);

    }

    public Storage() {
    }

    public static Storage createStorage() {
        slots.put(SizeEnum.SMALL.getSize(), 10);
        slots.put(SizeEnum.MEDIUM.getSize(), 0);
        slots.put(SizeEnum.BIG.getSize(), 0);
        return new Storage(20);
    }

    public Ticket save(Bag bag) {
        return save(bag, SizeEnum.SMALL.getSize());
    }

    public Ticket save(Bag bag, String chooseSize) {
        if (slots.get(chooseSize) > 0) {
            if (bag==null||chooseSize.charAt(0) <= bag.getSize().charAt(0)) {
                Ticket ticket = new Ticket();
                storage.put(ticket, bag);
                slots.put(chooseSize,slots.get(chooseSize)-1);
                return ticket;
            }else {
                throw new IllegalArgumentException("Cannot save the bag:"+bag.getSize()+" "+chooseSize);
            }
        }else {
            throw new IllegalArgumentException("Insufficient capacity");
        }
    }

    public Bag retrieve(Ticket ticket) {
        if (storage.containsKey(ticket)) {
            Bag bag = storage.get(ticket);
            //ticket size 表示slot的
            slots.put(ticket.getSize(),slots.get(ticket.getSize())+1);
            return storage.remove(ticket);
        } else {
            throw new IllegalArgumentException("Invalid Ticket");
        }
    }

    public void initSlots(String size, Integer num) {
        slots.put(size, num);
    }
}
