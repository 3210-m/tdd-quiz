package com.twuc.webApp.entity;

import com.twuc.webApp.enums.SizeEnum;

public class Ticket {
    private String size;

    public Ticket(String size) {
        this.size = size;
    }

    public Ticket() {
        this(SizeEnum.SMALL.getSize());
    }

    public String getSize() {
        return size;
    }
}
