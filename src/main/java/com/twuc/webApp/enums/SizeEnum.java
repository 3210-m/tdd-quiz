package com.twuc.webApp.enums;

public enum SizeEnum {
    SMALL("s"),MEDIUM("m"),BIG("b");

    private final String size;

    SizeEnum( String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}
