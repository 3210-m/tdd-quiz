package com.twuc.webApp.entity;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleTest {

    @Test
    void should_get_ticket_when_save_bag() {
        Storage storage = Storage.createStorage();
        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
    }
    @Test
    void should_get_ticket_when_save_nothing() {
        Storage storage = Storage.createStorage();
        Bag nothing = null;
        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_saved_bag_when_retrieving_bag_using_useful_ticket()  {
        Storage storage = Storage.createStorage();
        Bag expectBag = new Bag();
        Ticket ticket = storage.save(expectBag);
        Bag actualBag = storage.retrieve(ticket);
        assertSame(expectBag,actualBag);
    }
    @Test
    void should_get_saved_bags_when_retrieving_bag_using_useful_tickets() {
        Storage storage = Storage.createStorage();

        Bag expectBag1 = new Bag();
        Ticket ticket1 = storage.save(expectBag1);

        Bag expectBag2 = new Bag();
        Ticket ticket2 = storage.save(expectBag2);


        Bag actualBag1 = storage.retrieve(ticket1);
        assertSame(expectBag1,actualBag1);
    }

    @Test
    void should_get_error_message_when_retrieving_saved_bag_using_invalid_ticket() {
        Storage storage = Storage.createStorage();
        Bag expectBag = new Bag();
        Ticket ticket = storage.save(expectBag);

        Ticket invalidTicket = new Ticket();
        assertThrows(IllegalArgumentException.class,()->storage.retrieve(invalidTicket),"Invalid Ticket");
    }

    @Test
    void should_get_nothing_when_save_nothing_and_retrieve_bag_use_valid_ticket() {
        Storage storage = Storage.createStorage();
        Bag nothing = null;
        Ticket ticket = storage.save(nothing);

        Bag retrieveBag = storage.retrieve(ticket);

        assertSame(nothing,retrieveBag);
    }

    @Test
    void should_get_a_ticket_when_saving_bag_to_empty_storage() {
        Storage storage = new Storage(2);
        Ticket ticket = storage.save(new Bag());

        assertNotNull(ticket);
    }

    @Test
    void should_get_errorMassage_when_save_bag_to_full_storage() {
        //Given
        Storage storage = new Storage(2);
        storage.save(new Bag());
        storage.save(new Bag());

        //when,then
        assertThrows(IllegalArgumentException.class,()->storage.save(new Bag()),"Insufficient capacity");
    }


    @Test
    void should_get_one_ticket_and_one_errorMessage_when_save_bag_to_storage_with_1_capacity() {
        //Giving
        Storage storage = new Storage(1);
        //when
        Ticket ticket = storage.save(new Bag());
        //then
        assertNotNull(ticket);
        assertThrows(IllegalArgumentException.class,()->storage.save(new Bag()),"Insufficient capacity");
    }

    @Test
    void should_get_one_ticket_when_save_bag_to_bigger_size_slot() {
        Storage storage = new Storage();
        storage.initSlots("b",1);

        Bag bag = new Bag("m");

        Ticket ticket = storage.save(bag,"b");

        assertNotNull(ticket);
    }
}
